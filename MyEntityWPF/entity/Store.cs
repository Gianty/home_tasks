﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace entity
{
    [Table("Customers")]
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Cnum { get; set; }

        public string Cname { get; set; }
        public string City { get; set; }
        public int Rating { get; set; }

        public int Snum { get; set; }
        public virtual Salesperson Salesperson { get; set; }

        public virtual ICollection<Order> Orders { get; set; } = new List<Order>();
    }

    [Table("Salespeople")]
    public class Salesperson
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Snum { get; set; }
        
        public string Sname { get; set; }
        public string City { get; set; }
        public decimal Comm { get; set; }

        public virtual ICollection<Order> Orders { get; set; } = new List<Order>();
    }

    [Table("Orders")]
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Onum { get; set; }

        public DateTime Odate { get; set; }
        public decimal Amt { get; set; }

        public int? Snum { get; set; }
        public virtual Salesperson Salesperson { get; set; }

        public int? Cnum { get; set; }
        public virtual Customer Customer { get; set; }
        public override string ToString()
        {
            return $"Order #{Onum} Odate={Odate.ToShortDateString()} Snum={Snum} Cnum={Cnum} Amt={Amt}";
        }
    }

    public class StoreContextInitializer : DropCreateDatabaseAlways<StoreContext>
    {
        protected override void Seed(StoreContext db)
        {
            var salespeople = new List<Salesperson>()
            {
                new Salesperson {Snum = 1001, Sname = "Peel", City = "London", Comm = 0.12M},
                new Salesperson {Snum = 1002, Sname = "Serres", City = "San Jose", Comm = 0.13M},
                new Salesperson {Snum = 1004, Sname = "Motika", City = "London", Comm = 0.11M},
                new Salesperson {Snum = 1007, Sname = "Rifkin", City = "Barcelona", Comm = 0.15M},
                new Salesperson {Snum = 1003, Sname = "Axelrod", City = "New York", Comm = 0.1M}
            };
            db.Salespeople.AddRange(salespeople);

            var customers = new List<Customer>()
            {
                new Customer {Cnum=2001, Cname="Hoffman", City="London", Rating=100, Salesperson=salespeople[0] },
                new Customer {Cnum=2002, Cname="Giovanni", City="Rome",Rating=200, Salesperson=salespeople[4] },
                new Customer {Cnum=2003, Cname="Liu", City="San Jose", Rating=200, Salesperson=salespeople[1] },
                new Customer {Cnum=2004, Cname="Grass", City="Berlin", Rating=300, Salesperson=salespeople[1] },
                new Customer {Cnum=2006, Cname="Clemens", City="London", Rating=100, Salesperson=salespeople[0] },
                new Customer {Cnum=2008, Cname="Cisneros", City="San Jose", Rating=300, Salesperson=salespeople[3] },
                new Customer {Cnum=2007, Cname="Pereira", City="Rome", Rating=100, Salesperson=salespeople[2] }
            };

            db.Customers.AddRange(customers);

            var o = new List<Order>()
            {
                new Order {Onum=3001, Amt = 18.69M, Customer = customers[5], Salesperson = salespeople[3], Odate = new DateTime(1990, 03, 10)},
                new Order {Onum=3003, Amt = 767.19M, Customer = customers[0], Salesperson = salespeople[0], Odate = new DateTime(1990, 03, 10)},
                new Order {Onum=3002, Amt = 1900.1M, Customer = customers[6], Salesperson = salespeople[2], Odate = new DateTime(1990, 03, 10)},
                new Order {Onum=3005, Amt = 5160.45M, Customer = customers[2], Salesperson = salespeople[1], Odate = new DateTime(1990, 03, 10)},
                new Order {Onum=3006, Amt = 1098.16M, Customer = customers[5], Salesperson = salespeople[3], Odate = new DateTime(1990, 03, 10)},
                new Order {Onum=3009, Amt = 1713.23M, Customer = customers[1], Salesperson = salespeople[4], Odate = new DateTime(1990, 03, 10)},
                new Order {Onum=3007, Amt = 75.75M, Customer = customers[3], Salesperson = salespeople[1], Odate = new DateTime(1990, 04, 10)},
                new Order {Onum=3008, Amt = 4723M, Customer = customers[4], Salesperson = salespeople[0], Odate = new DateTime(1990, 05, 10)},
                new Order {Onum=3010, Amt = 1309.95M, Customer = customers[3], Salesperson = salespeople[1], Odate = new DateTime(1990, 06, 10)},
                new Order {Onum=3011, Amt = 9891.88M, Customer = customers[4], Salesperson = salespeople[0], Odate = new DateTime(1990, 06, 10)},
                new Order {Onum=3004, Amt = 195.6M, Customer = customers[2], Salesperson = salespeople[4], Odate = new DateTime(1990, 04, 10)},

            };

            db.Orders.AddRange(o);
            db.SaveChanges();
        }
    }

    public class StoreContext : DbContext
    {
        static StoreContext()
        {
           // Database.SetInitializer(new StoreContextInitializer());
        }

        public StoreContext()
            : base("StoreDB")
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Salesperson> Salespeople { get; set; }
        public DbSet<Order> Orders { get; set; }
    }

}
