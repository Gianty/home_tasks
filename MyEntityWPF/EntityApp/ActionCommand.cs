﻿using System;
using System.Windows.Input;

namespace EntityApp
{
    public class ActionCommand: ICommand
    {
        public ActionCommand(Action action)
        {
            _action = action;
            IsExecutable = action != null;
        }

        private Action _action;
        private bool _isExecutable;

        public bool IsExecutable
        {
            get { return _isExecutable; }
            set
            {
                _isExecutable = value;
                CanExecuteChanged?.Invoke(this, new EventArgs());
            }
        }


        public bool CanExecute(object parameter)
        {
            return IsExecutable;
        }

        public void Execute(object parameter)
        {
            if(CanExecute(parameter))
                _action();
        }

        public event EventHandler CanExecuteChanged;
    }
}