﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using entity;

namespace EntityApp
{
    public class ViewModel: INotifyPropertyChanged, IDisposable
    {
        private entity.StoreContext _storeContext;

        public ObservableCollection<Customer> Customers { get; set; }
        public ObservableCollection<Salesperson> Salespeople{ get; set; }
        public ObservableCollection<Order> Orders { get; set; }


        public ActionCommand Save { get; set; }
        public ActionCommand SearchCustomers { get; set; }
        public ActionCommand SearchOrders { get; set; }
        public ActionCommand SearchSalespeople { get; set; }
        public string SearchCustomersPattern { get; set; } = "";
        public string SearchOrdersPattern { get; set; } = "";
        public string SearchSalespeoplePattern { get; set; } = "";

        private void SaveChanges()
        {
            _storeContext.SaveChanges();
        }

        public ViewModel()
        {
            _storeContext = new StoreContext();
            Customers = new ObservableCollection<Customer>(_storeContext.Customers);
            Salespeople = new ObservableCollection<Salesperson>(_storeContext.Salespeople);
            Orders = new ObservableCollection<Order>(_storeContext.Orders);

            //Save = new ActionCommand(SaveChanges);
            SearchCustomers = new ActionCommand(() =>
                {
                    Customers.Clear();
                    var newCustomers = _storeContext.Customers.Where(x => 
                        x.Cname.Contains(SearchCustomersPattern) ||
                        x.City.Contains(SearchCustomersPattern) || x.Salesperson.Sname.Contains(SearchCustomersPattern) ||
                        x.Rating.ToString().Equals(SearchCustomersPattern)
                    );

                    foreach (var customer in newCustomers)
                        Customers.Add(customer);
                }
            );

            SearchOrders = new ActionCommand(() =>
            {
                Orders.Clear();
                var newOrders = _storeContext.Orders.Where(x =>
                    x.Onum.ToString().Contains(SearchOrdersPattern) || x.Customer.Cname.Contains(SearchOrdersPattern) ||
                    x.Salesperson.Sname.Contains(SearchOrdersPattern) || x.Odate.ToString().Contains(SearchOrdersPattern) ||
                    x.Amt.ToString().Equals(SearchOrdersPattern)
                    );

                foreach (var order in newOrders)
                    Orders.Add(order);
            }
            );

            SearchSalespeople = new ActionCommand(() =>
            {
                Salespeople.Clear();
                var newSalespeople = _storeContext.Salespeople.Where(x =>
                    x.Sname.Contains(SearchSalespeoplePattern) || x.City.Contains(SearchSalespeoplePattern) ||
                    x.Comm.ToString().Equals(SearchSalespeoplePattern)
                    );

                foreach (var salesperson in newSalespeople)
                    Salespeople.Add(salesperson);
            }
            );
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Dispose()
        {
            _storeContext.Dispose();
        }
    }
}