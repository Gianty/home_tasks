﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToObjects
{
    class Program
    {
        static int GCD(int a, int b)
        {
            while (a > 0 && b > 0)
            {
                if (a > b)
                {
                    a %= b;
                }
                else b %= a;
            }

            return a > 0 ? a : b;
        }

        static int LCM(int a, int b)
        {
            return a / GCD(a, b) * b;
        }

        static int sum(int a)
        {
            int temp = 1;
            for (int i = 2; i < Math.Sqrt(a) + 1; i++)
            {
                if (a % i == 0)
                {
                    temp += (a / i == i) ? i : i + a / i;
                }
            }
            return temp;

        }

        static void Main(string[] args)
        {

            int firstTask = 1;

            for (int i = 1; i < 20; i++)
                if (firstTask%i != 0)
                    firstTask = LCM(firstTask, i);

            var firstTaskLinq = Enumerable.Range(1, 19).Aggregate((x, y) => x = LCM(y, x));

            Console.WriteLine("1.0)" + firstTask);
            Console.WriteLine("1.1)" + firstTaskLinq);


            int secondTask = 0;
            for (int a = 0; a < 1000; a++)
                for (int b = a + 1; b < 1000; b++)
                {
                    int c = 1000 - a - b;
                    if (a*a + b*b == c*c)
                        secondTask = a*b*c;
                }

            var secondTaskLinq =
                (from a in Enumerable.Range(0, 1000)
                 from b in Enumerable.Range(a, 1000 - a)
                 let c = 1000 - a - b
                 where c * c == a * a + b * b
                 select a * b * c
                 ).Sum(); 



            Console.WriteLine("2.0)" + secondTask);
            Console.WriteLine("2.1)" + secondTaskLinq);

            var dict = Enumerable.Range(1, 10000)
                .Select(x => new { key = x, value = sum(x) })
                .ToDictionary(x => x.key, y => y.value);

            int thirdTask = 0;
            for (int i = 1; i < 10000; i++)
            {
                if (dict[i] <= 10000 && i != dict[i] && i == dict[dict[i]])
                    thirdTask += i;
            }

//            var thirdTaskLinq = Enumerable.Range(1, 10000)
//                .Where(x => dict[x] <= 10000 && x != dict[x] && x == dict[dict[x]])
//                .Sum();

            var thirdTaskLinq =
                (from a in Enumerable.Range(1, 10000)
                    let b = sum(a)
                    where b <= 10000 && b != a && a == sum(b)
                    select a
                    ).Sum();


            Console.WriteLine("3.0)" + thirdTask);
            Console.WriteLine("3.1)" + thirdTaskLinq);



            var fourthTask = 0;

            for (int i = 2; i < 354500; i++)
            {
                var temp = i;
                var summ = 0;
                while (temp > 0)
                {
                    var x = temp%10;
                    summ += x*x*x*x*x;
                    temp /= 10;
                }
                if (summ == i)
                {
                    fourthTask += i;
                }
            }

            var fourthTaskLinq = Enumerable.Range(2, 354999)
                .AsParallel()
                .Where(x => x.ToString().Select(y => y - '0').Sum(y => y * y * y * y * y) == x)
                .Sum();

            Console.WriteLine("4.0)" + fourthTask);
            Console.WriteLine("4.1)" + fourthTaskLinq);

        }


    }
}
