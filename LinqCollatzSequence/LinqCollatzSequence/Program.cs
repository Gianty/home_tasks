﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqCollatzSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            long n = 19;

            Solutions sol = new Solutions();
            Console.WriteLine("Перебор:");
            var arr = sol.FindSequenceByBust(n);
            foreach (var num in arr)
                Console.Write(num + " ");
            Console.WriteLine();

            Console.WriteLine("Перебор с yield return:");
            var nums = sol.FindSequenceByYield(n);
            foreach (var num in nums)
                Console.Write(num + " ");
            Console.WriteLine();

            Console.WriteLine("Имплементация IEnumerable");
            nums = sol.FindSequenceByCollatzClass(n);
            foreach (var num in nums)
                Console.Write(num + " ");
            Console.WriteLine();

            Console.WriteLine("Длиннейшая последователиность из n чисел:");
            var longestSequence = sol.LongestSequence((int)n);
            nums = sol.FindSequenceByYield(longestSequence);
            Console.WriteLine(longestSequence);
            foreach (var num in nums)
                Console.Write(num + " ");
            Console.WriteLine();
        }
    }
}
