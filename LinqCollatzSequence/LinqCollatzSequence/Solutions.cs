﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqCollatzSequence
{
    class Solutions
    {
        Collatz col;

        public ArrayList FindSequenceByBust(long num)
        {
            long n = num;
            ArrayList seq = new ArrayList();
            seq.Add(n);
            while (n != 1) {                 
                n = (n % 2 == 0) ? n / 2 : n * 3 + 1;
                seq.Add(n);
            }

            return seq;

        }


        public IEnumerable<long> FindSequenceByYield(long num)
        {
            long n = num;
            while (n != 1)
            {
                yield return n;
                n = (n%2 == 0) ? n / 2 : n*3 + 1;
            }
            yield return 1;
        }

        public IEnumerable<long> FindSequenceByCollatzClass(long num)
        {
            col = new Collatz(num);
            return col;
        }

        public long LongestSequence(int num)
        {
                 return Enumerable.Range(1, num - 1)
                .AsParallel()
                .Select(x => new { num = x, count = FindSequenceByYield(x).Count() })
                .Aggregate((acc, x) => (acc.count < x.count) ? x : acc)
                .num;
        }

//
//        public IEnumerable<int> FindNumsByYield(int count = int.MaxValue)
//        {
//            for (int i = 12; i < count; i++)
//                if (i.IsExсess()) yield return i;
//        }
    }

    public class Collatz : IEnumerable<long>
    {
        public Collatz(long num)
        {
            _enumerator = new CollatzEnumerator(num);
        }

        private CollatzEnumerator _enumerator = null;

        public IEnumerator<long> GetEnumerator()
        {
            return _enumerator;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public class CollatzEnumerator : IEnumerator<long>
        {
            public CollatzEnumerator(long num)
            {
                _current = num * 2;
            }

            private long _current = 1;

            public long Current => _current;

            object IEnumerator.Current => _current;

            public void Dispose() { }

            public bool MoveNext()
            {
                if (_current < long.MaxValue && _current != 1)
                {
                    
                    //while (!_current.IsExсess()) _current++;
                    _current = (_current % 2 == 0) ? _current / 2 : _current * 3 + 1;


                    return true;
                }
                else return false;
            }

            public void Reset()
            {
                _current = 1;
            }
        }
    }
}
