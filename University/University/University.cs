﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University
{

    [Table("Students")]
    public class Student
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SId { get; set; }

        public string Name { get; set; }
        public int Age { get; set; }

        public int? GId { get; set; }
        public virtual Group Group { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, Age: {Age}, Group: {Group.Name}, Courses count: {Courses.Count}";
        }
    }

    [Table("Groups")]
    public class Group
    {
        [Key]
        public int GId { get; set; }

        public string Name { get; set; }

        public int? InId { get; set; }
        public virtual Institute Institute { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, Institute: {Institute.Name}";
        }
    }

    [Table("Courses")]
    public class Course
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CId { get; set; }

        public string Name { get; set; }
        public string Info { get; set; }

        public int? TId { get; set; }
        public virtual Teacher Teacher { get; set; }

        public virtual ICollection<Student> Students { get; set; }

        public override string ToString()
        {
            return $"Name: {Name} \nInfo: {Info} \nTeacher: {Teacher.Name} Students count:{Students.Count}";
        }
    }

    [Table("Teachers")]
    public class Teacher
    {
        [Key]
        public int TId { get; set; }

        public string Name { get; set; }

        public int? InId { get; set; }
        public virtual Institute Institute { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, Institute: {Institute.Name}";
        }
    }

    [Table("Institutes")]
    public class Institute
    {
        [Key]
        public int InId { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}";
        }
    }

    [Table("Marks")]
    public class Marks
    {
        [Key]
        public int Id { get; set; }

        public int Mark { get; set; }

        public int? SId { get; set; }
        public virtual Student Student { get; set; }

        public int? CId { get; set; }
        public virtual Course Course { get; set; }

        public override string ToString()
        {
            return $"Student: {Student.Name}, Course: {Course.Name}, Mark: {Mark}";
        }
    }

    public class UniversityContextInitializer : DropCreateDatabaseAlways<UniversityContext>
    {
        protected override void Seed(UniversityContext db)
        {
            //Имена и названия взяты из Гарри Поттера, но не следуют его канонам

            var i = new List<Institute>()
            {
                new Institute {Name = "Hogwarts School of Witchcraft and Wizardry"},
                new Institute {Name = "Durmstrang Institute"}
            };

            db.Institutes.AddRange(i);

            var t = new List<Teacher>()
            {
                new Teacher {Name = "Severus Snape", Institute = i[0]},
                new Teacher {Name = "Gilderoy Lockhart", Institute = i[1]},
                new Teacher {Name = "Minerva McGonagall", Institute = i[0]},
                new Teacher {Name = "Sybill Trelawney", Institute = i[1]},

            };

            db.Teachers.AddRange(t);

            var c = new List<Course>()
            {
                new Course
                {
                    CId = 1,
                    Name = "Potions",
                    Info = "In this course, students learn the correct way to brew potions",
                    Teacher = t[0]
                },
                new Course
                {
                    CId = 2,
                    Name = "Defence Against the Dark Arts",
                    Info = "In this course students learn how to magically defend themselves against Dark Creatures, " +
                           "the Dark Arts, and other dark charms",
                    Teacher = t[0]
                },
                new Course
                {
                    CId = 3,
                    Name = "Defence Against the Dark Arts",
                    Info = "In this course students learn how to magically defend themselves against Dark Creatures, " +
                           "the Dark Arts, and other dark charms",
                    Teacher = t[1]
                },
                new Course
                {
                    CId = 4,
                    Name = "Transfiguration",
                    Info = "It teaches the art of changing the form and appearance of an object",
                    Teacher = t[1]
                },
                new Course
                {
                    CId = 5,
                    Name = "Transfiguration",
                    Info = "It teaches the art of changing the form and appearance of an object",
                    Teacher = t[2]
                },
                new Course
                {
                    CId = 6,
                    Name = "Divination",
                    Info = "It teaches methods of divining the future, or gathering insights into future events, " +
                           "through various rituals and tools",
                    Teacher = t[3]
                }
            };

            var g = new List<Group>()
            {
                new Group {Name = "Gryffindor", Institute = i[0]},
                new Group {Name = "Hufflepuff", Institute = i[0]},
                new Group {Name = "Ravenclaw", Institute = i[1]},
                new Group {Name = "Slytherin", Institute = i[1]}
            };
            db.Groups.AddRange(g);

            var s = new List<Student>() { 
                 new Student {SId = 1, Name = "Harry Potter", Age = 16, Group = g[0], Courses = new List<Course> { c[1]}},
                 new Student {SId = 2, Name = "Ginevra Weasley", Age = 15, Group = g[0], Courses = new List<Course> { c[3]}},
                 new Student {SId = 3, Name = "Seamus Finnigan", Age = 15, Group = g[0], Courses = new List<Course> { c[0]}},
                 new Student {SId = 4, Name = "Hermione Granger", Age = 17, Group = g[1], Courses = new List<Course> { c[0]}},
                 new Student {SId = 5, Name = "Ronald Weasley", Age = 16, Group = g[1], Courses = new List<Course> { c[1]}},
                 new Student {SId = 6, Name = "Neville Longbottom", Age = 17, Group = g[1], Courses = new List<Course> { c[3]}},
                 new Student {SId = 7, Name = "Viktor Krum", Age = 18, Group = g[2], Courses = new List<Course> { c[2]}},
                 new Student {SId = 8, Name = "Draco Malfoy", Age = 17, Group = g[2], Courses = new List<Course> { c[5]}},
                 new Student {SId = 9, Name = "Pansy Parkinson", Age = 16, Group = g[2], Courses = new List<Course> { c[2]}},
                 new Student {SId = 10, Name = "Fleur Delacour", Age = 17, Group = g[3], Courses = new List<Course> { c[4]}},
                 new Student {SId = 11, Name = "Gregory Goyle", Age = 17, Group = g[3], Courses = new List<Course> { c[2]}},
                 new Student {SId = 12, Name = "Vincent Crabbe", Age = 16, Group = g[3], Courses = new List<Course> { c[5]}}
            };
            db.Students.AddRange(s);

            foreach (var course in c)
            {
                course.Students = s.Where(x => x.Courses.Contains(course)).ToList();
            }

            db.Courses.AddRange(c);

            var m = new List<Marks>
            {
                new Marks {Mark = 5, Course = c[1], Student = s[0]},
                new Marks {Mark = 4, Course = c[1], Student = s[0]},
                new Marks {Mark = 5, Course = c[3], Student = s[1]},
                new Marks {Mark = 3, Course = c[3], Student = s[1]},
                new Marks {Mark = 2, Course = c[0], Student = s[2]},
                new Marks {Mark = 2, Course = c[0], Student = s[3]},
                new Marks {Mark = 4, Course = c[0], Student = s[3]},
                new Marks {Mark = 3, Course = c[1], Student = s[4]},
                new Marks {Mark = 4, Course = c[3], Student = s[5]},
                new Marks {Mark = 5, Course = c[2], Student = s[6]},
                new Marks {Mark = 3, Course = c[2], Student = s[6]},
                new Marks {Mark = 4, Course = c[5], Student = s[7]},
                new Marks {Mark = 3, Course = c[2], Student = s[8]},
                new Marks {Mark = 5, Course = c[4], Student = s[9]},
                new Marks {Mark = 5, Course = c[2], Student = s[10]},
                new Marks {Mark = 2, Course = c[2], Student = s[10]},
                new Marks {Mark = 3, Course = c[5], Student = s[11]},
                new Marks {Mark = 3, Course = c[5], Student = s[11]}
            };
            db.Marks.AddRange(m);
            db.SaveChanges();
        }
    }

    public class UniversityContext : DbContext
    {
        static UniversityContext()
        {
            Database.SetInitializer<UniversityContext>(new UniversityContextInitializer());   
        }

        public UniversityContext() :base("UniversityDB")
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Institute> Institutes { get; set; }
        public DbSet<Marks> Marks { get; set; }
    }
}