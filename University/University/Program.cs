﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University
{
    class Program
    { 
        static void Main(string[] args)
        {
            using (var db = new UniversityContext())
            {
                db.Database.Log = (s => System.Diagnostics.Debug.WriteLine(s));

                Console.WriteLine("Institutes");
                foreach (var inst in db.Institutes)
                {
                    Console.WriteLine(inst);
                }
                Console.WriteLine();
                Console.WriteLine("Courses");
                foreach (var course in db.Courses)
                {
                    Console.WriteLine(course);
                }
                Console.WriteLine();
                Console.WriteLine("Teachers");
                foreach (var teacher in db.Teachers)
                {
                    Console.WriteLine(teacher);
                }
                Console.WriteLine();
                Console.WriteLine("Groups");
                foreach (var group in db.Groups)
                {
                    Console.WriteLine(group);
                }
                Console.WriteLine();
                Console.WriteLine("Students");
                foreach (var student in db.Students)
                {
                    Console.WriteLine(student);
                }
                Console.WriteLine();
                Console.WriteLine("Marks");
                foreach (var mark in db.Marks)
                {
                    Console.WriteLine(mark);
                }

                Console.ReadKey();
            }
        }
    }
}
