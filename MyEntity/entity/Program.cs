﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;

namespace entity
{
    public static class Tasks
    {
        //!!!Для каждого продавца вывести имя, сумму комиссионных по всем его продажам
        //и имя покупателя, осуществившего у него покупку на самую большую сумму
        public static void Task1(StoreContext db)
        {
            Console.WriteLine("Task 1)");
            var result = db.Salespeople
                .Select(s => new
                {
                    SName = s.Sname,
                    Comm_sum = s.Orders.Sum(o => o.Amt)*s.Comm,
                    CName = s.Orders.Where(o => o.Amt == s.Orders.Select(or => or.Amt).Max()).Select(o => o.Customer.Cname).Max()
                });

            foreach (var r in result)
                Console.WriteLine(r);
            Console.WriteLine();
        }


        //!!!Вывести имена продавцов, максимальная комиссия с одной продажи у которых > 500
        public static void Task2(StoreContext db)
        {
            Console.WriteLine("Task 2)");

            var maxComm = db.Salespeople.Select(s => s.Orders.Select(o => o.Amt * s.Comm));
            var result = db.Salespeople
                .Where(s => s.Orders.Select(o => o.Amt).Max() * s.Comm > 500)
                .Select(s => new { SName = s.Sname })
                .ToList();

            foreach (var r in result)
                Console.WriteLine(r);
            Console.WriteLine();
        }
      

        //!!!Вывести имена покупателей и продавцов, имеющих не менее 2-ух совместно проведенных заказов
        public static void Task3(StoreContext db)
        {
            Console.WriteLine("Task 3)");
            var result = db.Orders
                .GroupBy(o => new { o.Customer, o.Salesperson })
                .Where(x => x.Count() >= 2)
                .Select(x => new { x.Key.Customer.Cname, x.Key.Salesperson.Sname})
                .ToList();

            foreach (var r in result)
                Console.WriteLine(r);
            Console.WriteLine();

        }


        //!!!Вывести даты, в которые проводились заказы между продавцом и покупателем из одного города
        //и сумму таких заказов
        public static void Task4(StoreContext db)
        {
            Console.WriteLine("Task 4)");
            var result = db.Orders
                .Where(o => o.Customer.City == o.Salesperson.City)
                .GroupBy(o => o.Odate)
                .Select(x => new { Odate = x.Key, Sum = x.Sum(a => a.Amt) })
                .ToList();

            foreach (var r in result)
                Console.WriteLine(r);
            Console.WriteLine();
        }

        //!!!Вывести имена и рейтинги покупателей вместе с именами приписанных им продавцов,
        //упорядоченные по убыванию рейтинга
        public static void Task5(StoreContext db)
        {
            Console.WriteLine("Task 5)");
            var result = db.Customers
                .Select(c => new { c.Cname, c.Rating, c.Salesperson.Sname })
                .OrderByDescending(x => x.Rating)
                .ToList();

            foreach (var r in result)
                Console.WriteLine(r);
            Console.WriteLine();

        }
        

        //!!!Выбрать пары имен покуателей и продавцов, у которых сумма всех совместных заказов больше
        //максимальной суммы одиночного заказа (по всем заказам)
        public static void Task6(StoreContext db)
        {
            Console.WriteLine("Task 6)");
            var maxAmt = db.Orders.Max(x => x.Amt);
            var result = db.Orders
                .GroupBy(o => new {o.Customer.Cname, o.Salesperson.Sname})
                .Where(o => o.Sum(x => x.Amt) > maxAmt)
                .Select(o => new {CName = o.Key.Cname, SName = o.Key.Sname});

            foreach (var r in result)
                Console.WriteLine(r);
            Console.WriteLine();
        }


        //!!!Выбрать дату и общюю сумму заказов в эту дату для дат, у которых эта общая сумма заказов больше средней общей суммы по всем датам
        public static void Task7(StoreContext db)
        {
            Console.WriteLine("Task 7)");
            var sumAverage = db.Orders.GroupBy(o => o.Odate).Select(o => o.Sum(x => x.Amt)).Average();

            var result = db.Orders
                .GroupBy(o => o.Odate)
                .Where(o => o.Sum(x => x.Amt) > sumAverage)
                .Select(o => new {ODate = o.Key, Amt = o.Sum(x => x.Amt)});

            foreach (var r in result)
                Console.WriteLine(r);
            Console.WriteLine();
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            
            using (var db = new StoreContext())
            {
                db.Database.Log = (s => System.Diagnostics.Debug.WriteLine(s));
                db.Configuration.LazyLoadingEnabled = true;

                Tasks.Task1(db);

                Tasks.Task2(db);

                Tasks.Task3(db);

                Tasks.Task4(db);

                Tasks.Task5(db);

                Tasks.Task6(db);

                Tasks.Task7(db);

            }


            Console.ReadKey();

            
        }
    }
}
