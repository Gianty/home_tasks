﻿using System;
using System.Collections.Generic;

namespace LinqMethods
{
    public static class Methods
    {
        public static IEnumerable<T> Filter<T>
            (this IEnumerable<T> source, Func<T,bool>predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            return source.FilterImpl(predicate);
        }

        private static IEnumerable<T> FilterImpl<T>
            (this IEnumerable<T> source, Func<T, bool> predicate)
        {
            foreach (var item in source)
                if (predicate(item))
                    yield return item;
        }

        public static IEnumerable<T> Filter<T>
            (this IEnumerable<T> source, Func<T, int, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            return source.FilterImpl(predicate);
        }

        private static IEnumerable<T> FilterImpl<T>
            (this IEnumerable<T> source, Func<T,int, bool> predicate)
        {
            int index = 0;
            foreach (var item in source)
                if (predicate(item, index++))
                    yield return item;
        }

        public static int Length<T>
            (this IEnumerable<T>source)
        {
            var i = 0;
            foreach (var item in source)
                i++;
            return i;
        }

        public static T[] ToArray<T>
            (this IEnumerable<T> source)
        {
            var lst = new List<T>();
            foreach (var item in source)
                lst.Add(item);
            return lst.ToArray();
        }

        public static IEnumerable<TResult> Map<T,TResult>
            (this IEnumerable<T>source, Func<T,TResult> mapper)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (mapper == null)
                throw new ArgumentNullException(nameof(mapper));
            return MapImpl(source, mapper);
        }

        private static IEnumerable<TResult> MapImpl<T, TResult>
            (IEnumerable<T> source, Func<T, TResult> mapper)
        {
            foreach (var item in source)
                yield return mapper(item);
        }

        public static IEnumerable<TResult> Map<T, TResult>
            (this IEnumerable<T> source, Func<T, int, TResult> mapper)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (mapper == null)
                throw new ArgumentNullException(nameof(mapper));
            return MapImpl(source, mapper);
        }

        private static IEnumerable<TResult> MapImpl<T, TResult>
            (IEnumerable<T> source, Func<T, int, TResult> mapper)
        {
            int index = 0;
            foreach (var item in source)
                yield return mapper(item, index++);
        }

        public static T Fold<T>
            (this IEnumerable<T>source,Func<T,T,T> func)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (func == null)
                throw new ArgumentNullException(nameof(func));

            using (var e = source.GetEnumerator())
            {
                if (!e.MoveNext())
                    throw new InvalidOperationException("Source was empty");

                var cur = e.Current;
                while (e.MoveNext())
                    cur = func(cur, e.Current);
                return cur;
            }     
        }

        public static TAccumulate Fold<T, TAccumulate>
            (this IEnumerable<T> source, TAccumulate seed, Func<TAccumulate, T, TAccumulate> func)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (seed == null)
                throw new ArgumentNullException(nameof(seed));
            if (func == null)
                throw new ArgumentNullException(nameof(func));

            using (var e = source.GetEnumerator())
            {
                var cur = seed;
                while (e.MoveNext())
                    cur = func(cur, e.Current);
                return cur;
            }
        }

        public static TResult Fold<T, TAccumulate, TResult>
            (this IEnumerable<T> source, TAccumulate seed, Func<TAccumulate, T, TAccumulate> func, Func<TAccumulate, TResult> result)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (seed == null)
                throw new ArgumentNullException(nameof(seed));
            if (func == null)
                throw new ArgumentNullException(nameof(func));
            if (result == null)
                throw new ArgumentNullException(nameof(result));

            using (var e = source.GetEnumerator())
            {
                var cur = seed;
                while (e.MoveNext())
                    cur = func(cur, e.Current);
                return result(cur);
            }
        }
    }
}
