﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqMethods;

namespace LinqTests


{

    internal class ThrowingEnumerable : IEnumerable<int>
    {
        private class ThrowingEnumerator : IEnumerator<int>
        {
            public void Dispose()
            { }

            public bool MoveNext()
            {
                throw new NotImplementedException();
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }

            public int Current
            {
                get { throw new NotImplementedException(); }
            }


            object IEnumerator.Current
            {
                get { return Current; }
            }
        }

        private ThrowingEnumerator e = new ThrowingEnumerator();
        public IEnumerator<int> GetEnumerator()
        {
            return e;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    [TestClass]
    public class FilterTests
    {
        [TestMethod]
        public void SimpleFilterTest()
        {
            int[] numbers = {1, 2, 3, 4, 5, 6, 7};
            var result = numbers.Filter(x => x%2 == 0).ToArray();
            int[] ideal = {2, 4, 6};
            CollectionAssert.AreEqual(result, ideal);
            //CollectionAssert.AreEqual(result, ideal);
            //int lresult = result.Length();
            //int lideal = ideal.Length();
            
            /*var eresult = result.GetEnumerator();
            var eideal = ideal.GetEnumerator();
            bool b = true;
            for(int i=0; b; i++)
            {
                if (eresult.Current != (int)eideal.Current)
                    b = false;
                bool b1 = eresult.MoveNext();
                bool b2 = eideal.MoveNext();

                if (b1 != b2) b = false;
                if (!(b1 && b2)) break;

                //Assert.AreEqual(eresult.Current, eideal.Current);
                //Assert.AreEqual(eresult.MoveNext(),eideal.MoveNext());
            }
            Assert.IsTrue(b);*/
        }

        [TestMethod]
        public void FilterWithIndexTest()
        {
            int[] numbers = { 10, 0, 15, 8, 38, 90, 71 };
            var result = numbers.Filter((x, i) => i * 10  <= x).ToArray();
            int[] ideal = { 10, 90, 71 };
            CollectionAssert.AreEqual(result, ideal);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FilterNullSourceTest()
        {
            IEnumerable<int> e = null;
            var tmp=e.Filter(x => true);
            int k = 0;
            foreach (var i in tmp)
            {
                k++;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FilterWithIndexNullSourceTest()
        {
            IEnumerable<int> e = null;
            var tmp = e.Filter((x,y) => true);
            int k = 0;
            foreach (var i in tmp)
            {
                k++;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FilterNullPredicateTest()
        {
            int[] e = {1, 2, 3};
            Func<int, bool> p = null;
            var tmp = e.Filter(p);
            int k = 0;
            foreach (var i in tmp)
            {
                k++;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FilterWithIndexNullPredicateTest()
        {
            int[] e = { 1, 2, 3 };
            Func<int,int, bool> p = null;
            var tmp = e.Filter(p);
            int k = 0;
            foreach (var i in tmp)
            {
                k++;
            }
        }

        [TestMethod]
        public void FilterDeferredTest()
        {
            try
            {
                ThrowingEnumerable e = new ThrowingEnumerable();
                var tmp = e.Filter(x => true);
                tmp = e.Filter((x, y) => true);
            }
            catch(Exception)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void MapSideEffects()
        {
            int[] numbers = { 1, 2, 3 };
            int i = 0;
            var tmp = numbers.Map(x => i++);
            var tmp2 = numbers.Map(x => i++);
            i = 10;
            var tmp3 = numbers.Map(x => i++);

            CollectionAssert.AreEqual(tmp.ToArray(),new int[3] { 10,11,12});
            CollectionAssert.AreEqual(tmp2.ToArray(), new int[3] { 13, 14, 15 });
            i = 0;
            CollectionAssert.AreEqual(tmp3.ToArray(), new int[3] { 0, 1, 2 });
        }
    }

    [TestClass]
    public class MapTests
    {
        [TestMethod]
        public void MapTest()
        {
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7 };
            var result = numbers.Map(x => x * x).ToArray();
            int[] ideal = { 1, 4, 9, 16, 25, 36, 49 };
            CollectionAssert.AreEqual(result, ideal);
        }

        [TestMethod]
        public void MapWithIndexTest()
        {
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7 };
            var result = numbers.Map((x, i) => x * i).ToArray();
            int[] ideal = { 0, 2, 6, 12, 20, 30, 42 };
            CollectionAssert.AreEqual(result, ideal);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MapNullSourceTest()
        {
            IEnumerable<int> e = null;
            var tmp = e.Map(x => true);
            int k = 0;
            foreach (var i in tmp)
            {
                k++;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MapWithIndexNullSourceTest()
        {
            IEnumerable<int> e = null;
            var tmp = e.Map((x,y) => true);
            int k = 0;
            foreach (var i in tmp)
            {
                k++;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MapNullMapperTest()
        {
            int[] e = { 1, 2, 3 };
            Func<int, int> m = null;
            var tmp = e.Map(m);
            int k = 0;
            foreach (var i in tmp)
            {
                k++;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MapWithIndexNullMapperTest()
        {
            int[] e = { 1, 2, 3 };
            Func<int, int, int> m = null;
            var tmp = e.Map(m);
            int k = 0;
            foreach (var i in tmp)
            {
                k++;
            }
        }

        [TestMethod]
        public void MapDeferredTest()
        {
            try
            {
                ThrowingEnumerable e = new ThrowingEnumerable();
                var tmp = e.Map(x => true);
                tmp = e.Map((x,y) => true);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

    }

    [TestClass]
    public class FoldTests
    {
        [TestMethod]
        public void FoldTest()
        {
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7 };
            var result = numbers.Fold((x,y) => x + y);
            int ideal = 28;
            Assert.AreEqual(result, ideal);
        }

        [TestMethod]
        public void FoldWithSeedTest()
        {
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7 };
            var result = numbers.Fold(10, (x, y) => x + y);
            int ideal = 38;
            Assert.AreEqual(result, ideal);
        }

        [TestMethod]
        public void FoldWithResultFuncTest()
        {
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7 };
            var result = numbers.Fold(0,(x, y) => x + y, r => r * r);
            int ideal = 784;
            Assert.AreEqual(result, ideal);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FoldNullSourceTest()
        {
            IEnumerable<int> e = null;
            var tmp = e.Fold((x,y) => x + y);
            bool k = 0 == tmp;
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentNullException))]
        public void FoldWithSeedNullSourceTest()
        {
            IEnumerable<int> e = null;
            var tmp = e.Fold(0, (x, y) => x + y);
            bool k = 0 == tmp;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FoldWithResultFuncNullSourceTest()
        {
            IEnumerable<int> e = null;
            var tmp = e.Fold(0, (x, y) => x + y, r => r.ToString());
            string s = tmp;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FoldNullFuncTest()
        {
            int[] e = { 1, 2, 3 };
            Func<int, int, int> m = null;
            var tmp = e.Fold(m);
            bool k = 0 == tmp;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FoldWithSeedNullFuncTest()
        {
            int[] e = { 1, 2, 3 };
            Func<int, int, int> m = null;
            var tmp = e.Fold(0,m);
            bool k = 0 == tmp;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FoldWithResultFuncNullFuncTest()
        {
            int[] e = { 1, 2, 3 };
            Func<int, int, int> m = null;
            var tmp = e.Fold(0,m,r => r.ToString());
            string s = tmp;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FoldWithSeedNullSeedTest()
        {
            int[] e = { 1, 2, 3 };
            string s = null;
            var tmp = e.Fold(s,(x, y) => x + y);
            bool k = "test".Equals(tmp);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FoldWithResultFuncNullSeedTest()
        {
            int[] e = { 1, 2, 3 };
            string s = null;
            var tmp = e.Fold(s, (x, y) => x + y, res => res.ToString());
            bool k = "test".Equals(tmp);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FoldWithResultFuncNullResTest()
        {
            int[] e = { 1, 2, 3 };
            Func<int, int> r = null;
            var tmp = e.Fold(0, (x, y) => x + y, r);
            bool k = 0 == tmp;
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void FoldEmptySourceTest()
        {
            int[] e = new int[0];
            var tmp = e.Fold((x, y) => x + y);
            bool k = 0 == tmp;

        }

    }
}
