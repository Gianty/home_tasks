﻿1. Smoke On The Water - Deep Purple

2. Another Brick In The Wall Part 2 - Pink Floyd

3. Stairway To Heaven - Led Zeppelin

4. Bohemian Rhapsody - Queen

5. Nothing Else Mathers - Metallica

6. Highway To Hell - AC/DC
 
7. Back In Black - AC/DC
 
8. Paradise City - Guns N`Roses
 
9. Child In Time - Deep Purple
 
10. Sweet Child O`Mine - Guns N`Roses
 
11. Here I Go Again - Whitesnake
 
12. Smells Like Teen Spirit - Nirvana
 
13. Hotel California - Eagles
 
14. Money For Nothing - Dire Straits
 
15. Highway Star - Deep Purple
 
16. One - Metallica
 
17. Splitter Pine - DumDum Boys
 
18. Born To Be wild - Steppenwolf
 
19. You Shook Me All Night Long - AC/DC
 
20. Run to the Hills - Iron Maiden
 
21. The Unforgiven - Metallica
 
22. Fireball - Deep Purple
 
23. Lazy - Deep Purple
 
24. Born In The USA - Bruce Springsteen
 
25. Poison - Alice Cooper
 
26. Satisfaction - The Rolling Stones
 
27. Sultans Of Swing - Dire Straits
 
28. Thunderstruck - AC/DC
 
29. Money - Pink Floyd
 
30. Living On A Prayer - Bon Jovi
 
31. Eye Of The Tiger - Survivour
 
32. Perfect Stranger - Deep Purple
 
33. Paranoid - Black Sabbath
 
34. Comfortably Numb - Pink Floyd
 
35. Numer of the beast - Iron Maiden
 
36. Ace Of Spades - Motörhead
 
37. Knocking At Your Backdoor - Deep Purple
 
38. Hey Joe - Jimi Hendrix
 
39. Knockin On Heavens Door - Bob Dylan
 
40. I was Mad For Loving You - Kiss
 
41. Can`t Let You Go - Rainbow
 
42. Anuones Daughter - Deep Purple
 
43. House Of The Rising Sun - The Animals
 
44. Stargazer - Rainbow
 
45. Holy Diver - Dio
 
46. Whole Lotta Love - Led Zeppelin
 
47. Rockin`In The Free World - Neil Young
 
48. Imagine - John Lennon
 
49. Jailhouse Rock - Elvis Presley
 
50. Should I Stay Or Should I Go - The Clash
 
51. Killing in the name of - Rage against the machine
 
52. Were Not Gonna Take It - Twisted Sister
 
53. All Along The Watchctower - Jimi Hendrix
 
54. I Love Rock`N Roll - Joan Jett
 
55. 10 000 Lovers - TNT
 
56. The Ballroom Blitz - The Sweet
 
57. Whiskey In The Jar - Thin Lizzy
 
58. The Final Countdown - Europe
 
59. Like A Rolling Stone - Bob Dylan
 
60. The Trooper - Iron Maiden
 
61. With Or Whitout You - U2
 
62. Brothers In Arms - Dire Straits
 
63. We Will Rock You - Queen
 
64. Voodoo Child - Jimi Hendrix
 
65. Ziggy Stardust - David Bowie
 
66. Pride (In The Name Of Love) - U2
 
67. Jump - Van Halen
 
68. The Boys Are Back In Town - Thin Lizzy
 
69. Welcome To The Jungle - Guns N`Roses
 
70. I Wanna Rock - Twisted Sister
 
71. God Save The Queen - The Sex Pistols
 
72. Free Bird - Lynyrd Skynyrd
 
73. White Wedding - Billy Idol
 
74. Sympathy For The Devil - The Rolling Stones
 
75. You Really Got Me - The Kinks
 
76. Angie - The Rolling Stones
 
77. Can I Play With Madness - Iron Maiden
 
78. Bark At The Moon - Ozzy Osbourne
 
79. Paint It Black - The Rolling Stones
 
80. Every Breath You Take - Police
 
81. Hound Dog - Elvis Presley
 
82. Jumpin' Jack Flash - The Rolling Stones
 
83. Rock And Roll All Night - Kiss
 
84. Johnny B Goode - Chuck Berry
 
85. Honky Tonk Women - The Rolling Stones
 
86. Dreamer - Ozzy Osbourne
 
87. Great Balls Of Fire - Jerry Lee Lewis
 
88. Wonderwall - Oasis
 
89. Whiplash - Metallica
 
90. Beds Are Burning - Midnight Oil
 
91. Pour Some Sugar On Me - Def Leppard
 
92. Yesterday - Paul McCartney
 
93. Helter Skelter - The Beatles
 
94. Heartbreak Hotel - Elvis Presley
 
95. My Generation - The Who
 
96. Lithium - Nirvana
 
97. Heavens On Fire - Kiss
 
98. Rock & Roll - Led Zeppelin
 
99. Space Oddity - David Bowie
 
100. Rape Me - Nirvana