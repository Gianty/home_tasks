﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace RockSongs
{
    class Program
    {
        static void ManualProcess(string input, string xmlOutput)
        {
            using (var sr = new StreamReader(input))
            {
                var songs = new XElement("Songs");
                while (!sr.EndOfStream)
                {
                    var song = new XElement("Song");
                    var tmp = sr.ReadLine()?.Split('.', '-');
                    if (tmp != null && tmp.Length == 3)
                    {
                        song.Add(new XAttribute("Id", tmp[0]));
                        song.Add(new XAttribute("Artist", tmp[2].Trim(' ')));
                        song.Add(new XAttribute("Name", tmp[1].Trim(' ')));
                        songs.Add(song);
                    }
                }
                var doc = new XDocument();
                doc.Add(songs);
                doc.Save(xmlOutput);
            }
        }

        static void Textify(string xmlInput, string output)
        {
            var doc = XDocument.Load(xmlInput);
            using (var sw = new StreamWriter(output))
            {
                foreach (var item in doc.Root?.Elements())
                {
                    sw.WriteLine($"{item.Name} {item.Value}");
                    sw.WriteLine("Attributes");
                    foreach (var attr in item.Attributes())
                        sw.WriteLine($"{attr.Name}: {attr.Value}");
                    sw.WriteLine("Elements");
                    foreach (var e in item.Elements())
                        sw.WriteLine($"{e.Name}: {e.Value}");
                    sw.WriteLine();
                }
            }

        }

        static void Main()
        {
            ManualProcess("input.txt", "output.xml");

            Textify("output.xml", "output.txt");

            var songs = XDocument.Load("output.xml");


            //Задание 0
            var listOfArtists = songs.Root.Descendants()
                .OrderBy(x => x.Attribute("Artist").Value)
                .GroupBy(x => x.Attribute("Artist").Value)
                .Select(x => x.Key);

            Console.WriteLine("Список артистов без повторений");
            foreach (var artist in listOfArtists)
                Console.WriteLine(artist);
            Console.WriteLine();

            //Задание 1
            var maxOfSongs =
                songs.Root.Descendants()
                .GroupBy(x => x.Attribute("Artist").Value)
                .Where(x => x.Count() ==
                songs.Root.Descendants()
                .GroupBy(y => y.Attribute("Artist").Value)
                .Select(y => y.Count()).Max())
                .Select(x => $"{x.Key}: " + $"{x.Count()}")
                ;

            Console.WriteLine("Артист с наибольшим количеством песен");
            foreach (var song in maxOfSongs)
                Console.WriteLine(song);
            Console.WriteLine("");


            //Задание 2а
            var sortByCount =
                songs.Root.Descendants()
                    .GroupBy(x => x.Attribute("Artist").Value)
                    .OrderByDescending(x => x.Count())
                    .ThenBy(x => x.Key)
                    .Select(x => $"{x.Key}: " + $"{x.Count()}");

            Console.WriteLine("Сортировка по количеству песен");
            foreach (var artist in sortByCount)
                Console.WriteLine(artist);
            Console.WriteLine();

            //Задание 2б
            var sortByCountWithNames =
                (from song in songs.Root.Descendants()
                 group new { Name = song.Attribute("Name").Value, Id = song.Attribute("Id").Value }
                 by song.Attribute("Artist").Value
                 into g
                 let count = g.Count()
                 orderby count descending, g.Key
                 select g
                    );

            Console.WriteLine("Сортировка по количеству песен v.2");
            foreach (var artist in sortByCountWithNames)
            {
                Console.WriteLine($"{artist.Key}: " + $"{artist.Count()}");
                foreach (var song in artist)
                    Console.WriteLine($"\t{song.Name}");
            }
            Console.WriteLine();

            //Задание 2в

            var _songs = new XElement("Songs");
            foreach (var artist in sortByCountWithNames)
            {
                var _artist = new XElement("Artist");
                _artist.Add(new XAttribute("Name", artist.Key));
                _artist.Add(new XAttribute("Count", artist.Count()));
                foreach (var song in artist)
                {
                    var _song = new XElement("Song");
                    _song.Add(new XAttribute("Id", song.Id));
                    _song.Add(new XAttribute("Name", song.Name));
                    _artist.Add(_song);

                }
                _songs.Add(_artist);
            }
            var doc = new XDocument();
            doc.Add(_songs);
            doc.Save("Sorted.xml");

            Console.WriteLine("Где-то в этот момент сохранился документ 'Sorted.xml'");
            Console.WriteLine();

            //Задание 3
            var rockSongs =
                songs.Root.Descendants()
                .Where(x => x.Attribute("Name").Value.Contains("Rock"))
                .Select(x => x.Attribute("Name").Value);

            Console.WriteLine("Песни с 'Rock' в названии");
            foreach (var song in rockSongs)
                Console.WriteLine(song);
            Console.WriteLine();

            var rockArtists =
                songs.Root.Descendants()
                    .Where(x => x.Attribute("Name").Value.Contains("Rock"))
                    .Select(x => $"{x.Attribute("Artist").Value} – " + $"{x.Attribute("Name").Value}");

            Console.WriteLine("Артисты с песнями из предыдущего пункта");
            foreach (var song in rockArtists)
                Console.WriteLine(song);
            Console.WriteLine();



        }
    }
}
