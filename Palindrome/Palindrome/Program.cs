﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            //int max = 0;
            
            //for(int i = 100; i < 1000; i++)
            //    for (int j = i; j < 1000; j++)
            //        if ((i * j).IsPalindrome() && i * j > max)
            //            max = i * j;
            //Console.WriteLine(max);


            var pal = Enumerable.Range(100, 900)
                .Where(x => (Enumerable.Range(x, 1000 - x).Where(y => (x * y).IsPalindrome())).Any())
                .Select(x => (Enumerable.Range(x, 1000 - x).Where(y => (x * y).IsPalindrome()).Select(y => x * y)).Max()).Max();

            var palq =
                (from a in Enumerable.Range(100, 900)
                from b in Enumerable.Range(a, 1000 - a)
                where (a * b).IsPalindrome()
                select (a * b)).Max();

            Console.WriteLine(pal);
            Console.WriteLine(palq);
            Console.ReadKey();

        }

        
    }

    public static class Extensions
    {
        public static bool IsPalindrome(this int num)
        {
            int temp = num;
            int rev = 0;

            while (temp > 0)
            {
                rev = rev * 10 + temp % 10;
                temp /= 10;
            }


            return (rev == num) ? true : false;
        }
    }
}
