﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BinaryOperator = System.Tuple<int, System.Func<decimal, decimal, decimal>>;
using UnaryOperator = System.Tuple<int, System.Func<decimal, decimal>>;

namespace Calculator
{
    public class Expressions
    {
        public Dictionary<string, BinaryOperator> binaryOperators
            = new Dictionary<string, BinaryOperator>()
            {
            { "+",new BinaryOperator(0,(x,y)=>x+y )},
            { "-",new BinaryOperator(0,(x,y)=>x-y )},
            { "×",new BinaryOperator(1,(x,y)=>x*y )},
            { "÷",new BinaryOperator(1,(x,y)=>x/y )},
            };
        public Dictionary<string, UnaryOperator> unaryOperators
            = new Dictionary<string, UnaryOperator>()
            {
            {"sqrt",new UnaryOperator(3,x=>(decimal)Math.Sqrt((double)x)) },
            {"abs",new UnaryOperator(3,x=>(decimal)Math.Abs((double)x)) },
            {"±",new UnaryOperator(3,x=> - 1 * x) }
            };

        public List<string> Tokenize(string input)
        {
            foreach (var c in "()+-×÷±")
                input = input.Replace(c.ToString(), " " + c + " ");

            var tokens = input.Split(" ".ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

            if (tokens[0] == "-")
                tokens.Insert(0, "0");

            for (int i = 1; i < tokens.Count - 1; i++)
                if (tokens[i] == "-" && "(+-×÷".Contains(tokens[i - 1]))// Regex.Match(tokens[i], "[+-*/]").Success)// 
                {
                    tokens.Insert(i, "(");
                    tokens.Insert(i + 1, "0");
                    tokens.Insert(i + 4, ")");
                }
            return tokens;
        }

        public int GetPrecedence(string op)
        {
            if (binaryOperators.ContainsKey(op))
                return binaryOperators[op].Item1;
            else if (unaryOperators.ContainsKey(op))
                return unaryOperators[op].Item1;
            else
                throw new ArgumentException("Invalid operation {0}!", op);
            /*switch(op)
            {
                case "+":
                case "-":
                    return 0;
                case "*":
                case "/":
                    return 1;
                case "^":
                    return 2;
                default:
                    throw new ArgumentException("Invalid operation {0}!", op);
            }*/
        }

        public List<string> ToRpn(List<string> tokens)
        {
            List<string> rpn = new List<string>();
            Stack<string> rpnStack = new Stack<string>();

            foreach (var token in tokens)
            {
                if (token == "(")
                    rpnStack.Push(token);
                else if (token == ")")
                {
                    while (rpnStack.Count > 0 && rpnStack.Peek() != "(")
                        rpn.Add(rpnStack.Pop());
                    rpnStack.Pop();
                }
                else if (unaryOperators.ContainsKey(token) || binaryOperators.ContainsKey(token))
                {
                    int precedence = GetPrecedence(token);
                    while (rpnStack.Count > 0 && rpnStack.Peek() != "(" &&
                        GetPrecedence(rpnStack.Peek()) >= precedence)
                        rpn.Add(rpnStack.Pop());
                    rpnStack.Push(token);
                }
                else rpn.Add(token);
            }
            while (rpnStack.Count > 0)
                rpn.Add(rpnStack.Pop());

            return rpn;
        }

        public string Evaluate(List<string> rpn)
        {
            decimal value = 0;
            try
            {
                Stack<decimal> values = new Stack<decimal>();
                foreach (var token in rpn)
                {
                    if (binaryOperators.ContainsKey(token))
                    {
                        decimal left, right;
                        right = values.Pop();
                        left = values.Pop();
                        values.Push(binaryOperators[token].Item2(left, right));
                    }
                    else if (unaryOperators.ContainsKey(token))
                    {
                        decimal tmp;
                        tmp = values.Pop();
                        values.Push(unaryOperators[token].Item2(tmp));
                    }
                    else
                        values.Push(decimal.Parse(token));
                }
                value = Math.Round(values.Pop(), 17, MidpointRounding.AwayFromZero);
            }
            catch (System.InvalidOperationException)
            {
                value = 0;
            }
            catch (DivideByZeroException)
            {
                return "Division by zero!";
            }
            catch (System.OverflowException)
            {
                return "Too difficult for me :(";
            }
            catch (System.FormatException)
            {
                value = 0;
            }

            return (Math.Round(value) == value) ? Math.Round(value).ToString() : value.ToString();
        }
    }
}

