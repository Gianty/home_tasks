﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        Expressions expressions = new Expressions();
        bool operationIsUsed = false;
        bool commaIsUsed = false;
        int openBracketsAmount = 0;
        string lastButton = "0";
        private string divByZero = "Division by zero!";
        private string complOp = "Too difficult for me :("; //Когда калькулятор отказывается высчитывать такие числа


        public MainWindow()
        {
            InitializeComponent();
        }

        private void button0_Click(object sender, RoutedEventArgs e)
        {
            if (textBox.Text.Equals(divByZero) || textBox.Text.Equals(complOp))
            {
                textBox.Text = "0";
                textBox1.Text = "";
                lastButton = "0";
                operationIsUsed = false;
                return;
            }

            var currentButton = ((Button)sender).Content.ToString();

            if (textBox.Text == "0")
            {
                textBox.Text = currentButton;
                lastButton = ((Button)sender).Content.ToString();
            }
            else if (!lastButton.Equals(")"))
            {
                textBox.Text += currentButton;
                lastButton = ((Button)sender).Content.ToString();
            }
            operationIsUsed = false;
            
        }

        private void digitButtonClick(object sender, RoutedEventArgs e)
        {
            var currentButton = ((Button)sender).Content.ToString();
            if (textBox.Text.Equals(divByZero) || textBox.Text.Equals(complOp))
            {
                textBox.Text = currentButton;
                textBox1.Text = "";
                lastButton = currentButton;
                operationIsUsed = false;
                return;
            }

            if (textBox.Text == "0")
            {
                textBox.Text = currentButton;
                lastButton = ((Button)sender).Content.ToString();
            }
            else if (!lastButton.Equals(")"))
            {
                textBox.Text += currentButton;
                lastButton = ((Button)sender).Content.ToString();
            }
            operationIsUsed = false;
        }

        private void binaryOperationButtonClick(object sender, RoutedEventArgs e)
        {
            if (textBox.Text.Equals(divByZero))
            {
                buttonClear_Click(sender, e);
                return; 
            }
            var currentButton = ((Button)sender).Content.ToString();

            if (openBracketsAmount > 0 && !operationIsUsed)
            {
                textBox.Text += currentButton;
                lastButton = currentButton;
            }
            else if (operationIsUsed)
            {
                if (!textBox.Text.Last().Equals('('))
                { 
                    textBox.Text = textBox.Text.Substring(0, textBox.Text.Length - 1);
                    textBox.Text += currentButton;
                }  
            }
            else
            {
                //decimal dec = expressions.Evaluate(expressions.ToRpn(expressions.Tokenize(textBox.Text)));
                textBox1.Text =  expressions.Evaluate(expressions.ToRpn(expressions.Tokenize(textBox.Text)));
                textBox.Text += currentButton;
                lastButton = currentButton;
            }
            operationIsUsed = true;
            commaIsUsed = false;
            
        }

        private void buttonEquals_Click(object sender, RoutedEventArgs e)
        {
            if (textBox.Text.Equals(divByZero) || textBox.Text.Equals(complOp))
            {
                buttonClear_Click(buttonClear, e);
                return;
            }
            if (lastButton.Equals("("))
            {
                textBox.Text += 0;
            }
            for (var i = openBracketsAmount; i > 0; i--)
            {
                textBox.Text += ")";
                openBracketsAmount--;
            }
            textBox1.Text = textBox.Text;
            //decimal dec = expressions.Evaluate(expressions.ToRpn(expressions.Tokenize(textBox.Text)));
            textBox.Text = expressions.Evaluate(expressions.ToRpn(expressions.Tokenize(textBox.Text)));
            operationIsUsed = false;
            commaIsUsed = textBox.Text.Contains(',') ? true : false;

            openBracketsAmount = 0;
            lastButton = ((Button)sender).Content.ToString();
        }

        private void buttonClear_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = "0";
            textBox1.Text = "";
            operationIsUsed = false;
            commaIsUsed = false;
            openBracketsAmount = 0;
            lastButton = ((Button)sender).Content.ToString();
        }

        private void buttonComma_Click(object sender, RoutedEventArgs e)
        {
            if (textBox.Text.Equals(divByZero) || textBox.Text.Equals(complOp))
            {
                buttonClear_Click(sender, e);
                return; 
            }
            if (!commaIsUsed && !lastButton.Equals(")"))
            {
                textBox.Text += ((Button)sender).Content.ToString();
                lastButton = ((Button)sender).Content.ToString();
                commaIsUsed = true;
            }
        }

        private void buttonNegate_Click(object sender, RoutedEventArgs e)
        {
            if (textBox.Text.Equals(divByZero) || textBox.Text.Equals(complOp))
            {
                buttonClear_Click(sender, e);
                return;
            }
            var currentButton = ((Button)sender).Content.ToString();
            string temp = textBox.Text;
            int index = temp.LastIndexOfAny("+-×÷".ToCharArray());       
            if (index != -1 && !temp.Last().Equals(temp.ElementAt(index)) && temp.ElementAt(index + 1).Equals(currentButton.ElementAt(0)))
                temp = temp.Remove(index + 1, 1);
            else if(temp.ElementAt(0).Equals(currentButton.ElementAt(0)))
                temp = temp.Remove(index + 1, 1);
            else
                temp = temp.Insert(index + 1, "±");
            textBox.Text = temp;
        }

        private void buttonLBracket_Click(object sender, RoutedEventArgs e)
        {
            if (textBox.Text.Equals(divByZero) || textBox.Text.Equals(complOp))
            {
                buttonClear_Click(sender, e);
                return; ;
            }
            lastButton = ((Button)sender).Content.ToString();
            if (textBox.Text == "0")
            {
                operationIsUsed = true;
                textBox.Text = lastButton;
                openBracketsAmount++;
            }
            else if (operationIsUsed)
            {
                textBox.Text += lastButton;
                openBracketsAmount++;
            }
        }

        private void buttonRBracket_Click(object sender, RoutedEventArgs e)
        {
            if (textBox.Text.Equals(divByZero) || textBox.Text.Equals(complOp))
            {
                buttonClear_Click(sender, e);
                return; ;
            }
            var currentButton = ((Button)sender).Content.ToString();
            if (openBracketsAmount > 0 &&! operationIsUsed)
            {
                textBox.Text += currentButton;
                openBracketsAmount--;
                lastButton = currentButton;
            }
        }
    }
}

