﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqExcessiveNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 10;
            ExcessiveNumbers ex = new ExcessiveNumbers();

            var arr = ex.FindNumsByBust(n);
            Console.WriteLine("Перебор:");
            foreach (var num in arr) Console.Write(num + " ");
            Console.WriteLine();

            Console.WriteLine("Явная реализация интерфейса:");
            var nums = ex.FindNumsByExcessClass(n);
            foreach (var num in nums) Console.Write(num + " ");
            Console.WriteLine();

            Console.WriteLine("Yield return:");
            nums = ex.FindNumsByYield().Take(10);
            foreach (var num in nums) Console.Write(num + " ");
            Console.WriteLine();

            Console.WriteLine("Range с фильрацией:");
            nums = ex.FindNumsByRange(10);
            foreach (var num in nums) Console.Write(num + " ");
            Console.WriteLine();

            Console.ReadKey();


        }
    }


}
