﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqExcessiveNumbers
{
    static class Extensions
    {
        public static bool IsExсess(this int num)
        {
            int sum = 1;
            for (int i = 2; i < num / 2 + 1; i++)
                if (num % i == 0)
                    sum += i;

            return sum > num;
        }
    }
}
