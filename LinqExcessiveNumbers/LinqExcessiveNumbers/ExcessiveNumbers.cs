﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqExcessiveNumbers
{
    class ExcessiveNumbers
    {
        private Excess exc = new Excess();

        public ArrayList FindNumsByBust(int num)
        {
            ArrayList arr = new ArrayList();
            int i = 12;

            while (arr.Count < num)
            {
                if (i.IsExсess())
                    arr.Add(i);
                i++;
            }

            return arr;
        }

        public IEnumerable<int> FindNumsByExcessClass(int num)
        {
            return exc.Take(num);
        }

        public IEnumerable<int> FindNumsByRange(int num)
        {
            return
                Enumerable.Range(12, 1000000)
                    .Where(n => (Enumerable.Range(2, n / 2 + 1).Where(div => n % div == 0).Sum() > n))
                    .Take(num);
        }

        public IEnumerable<int> FindNumsByYield(int count = int.MaxValue)
        {
            for (int i = 12; i < count; i++)
                if (i.IsExсess()) yield return i;
        }


    }

    public class Excess : IEnumerable<int>
    {
        private ExcessEnumerator _enumerator = new ExcessEnumerator();

        public IEnumerator<int> GetEnumerator()
        {
            return _enumerator;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public class ExcessEnumerator : IEnumerator<int>
        {
            private int _current = 11;

            public int Current => _current;

            object IEnumerator.Current => _current;

            public void Dispose() { }

            public bool MoveNext()
            {
                if (_current < int.MaxValue)
                {
                    _current++;
                    while (!_current.IsExсess()) _current++;


                    return true;
                }
                else return false;
            }

            public void Reset()
            {
                _current = 0;
            }
        }
    }
}
